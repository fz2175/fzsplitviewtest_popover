//
//  FZRightViewController.h
//  FZSplitViewTest
//
//  Created by Fan Zhang on 6/5/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FZRightViewController : UIViewController <UISplitViewControllerDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) NSString *displayStr;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
